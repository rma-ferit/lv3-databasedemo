package hr.ferit.brunozoric.database.persistence

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import hr.ferit.brunozoric.database.MyApplication
import hr.ferit.brunozoric.database.model.Book

@Database(version = 1, entities = arrayOf(Book::class), exportSchema = false)
abstract class BookDatabase : RoomDatabase() {

    abstract fun bookDao(): BookDao

    companion object {
        private const val NAME = "book_database"
        private var INSTANCE: BookDatabase? = null

        fun getInstance(): BookDatabase {
            if(INSTANCE == null) {

                INSTANCE = Room.databaseBuilder(
                    MyApplication.ApplicationContext,
                    BookDatabase::class.java,
                    NAME)
                    .allowMainThreadQueries() // Not for real apps!
                    .build()
            }
            return INSTANCE as BookDatabase
        }
    }
}