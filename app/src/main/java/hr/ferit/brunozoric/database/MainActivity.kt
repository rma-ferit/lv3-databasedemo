package hr.ferit.brunozoric.database

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import hr.ferit.brunozoric.database.model.Book
import hr.ferit.brunozoric.database.persistence.BookDatabase
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*

class MainActivity : AppCompatActivity() {

    val booksDao = BookDatabase.getInstance().bookDao()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        addBookAction.setOnClickListener{addBook()}
        displayBooks()
    }

    private fun displayBooks() {
        booksDisplay.adapter = ArrayAdapter<Book>(
            this,
            android.R.layout.simple_list_item_1,
            booksDao.getAll())
    }

    private fun addBook(){
        val index = Random().nextInt(100)
        val book = Book(0, "Title$index", "Author$index")
        booksDao.insert(book)
        displayBooks()
    }
}
