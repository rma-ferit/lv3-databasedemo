package hr.ferit.brunozoric.database.persistence

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import hr.ferit.brunozoric.database.model.Book

@Dao
interface BookDao{

    @Insert
    fun insert(book: Book);

    @Delete
    fun delete(book: Book);

    @Query("SELECT * FROM books")
    fun getAll(): List<Book>;

    @Query("SELECT * FROM books WHERE author = :author")
    fun getByAuthor(author: String): List<Book>;
}