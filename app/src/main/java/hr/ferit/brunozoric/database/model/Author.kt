package hr.ferit.brunozoric.database.model

data class Author(
    val id: Int,
    val name: String,
    val surname: String
)
